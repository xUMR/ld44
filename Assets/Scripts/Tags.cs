public static class Tags
{
    public static readonly string Player = "Player";
    public static readonly string Sword = "Sword";
    public static readonly string Shield = "Shield";
    public static readonly string CurrencyText = "CurrencyText";
    public static readonly string ContinueButton = "ContinueButton";
}
