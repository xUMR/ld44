using UnityEngine;

public class Movement : MonoBehaviour
{
    // todo moving forward should be faster
    [SerializeField] private float _speed = 1;
    [SerializeField] private float _fastWalkSpeed = 1.3f;
    [SerializeField] private float _slowWalkSpeed = .85f;
    [SerializeField] private Rigidbody2D _body;

    private float _speedDefault;

    private bool _isDead;

    private Vector2 Input2D
    {
        get
        {
            if (!CompareTag(Tags.Player)) return Vector2.zero;

            var x = Input.GetAxisRaw("Horizontal");
            var y = Input.GetAxisRaw("Vertical");
            var fastWalk = Mathf.Max(1, Input.GetAxisRaw("Fire3") * _fastWalkSpeed);

            return new Vector2(x, y).normalized * fastWalk;
        }
    }

    private void Awake()
    {
        _speedDefault = _speed;
    }

    private void OnEnable()
    {
        Events.OnBlock += BlockSpeedAdjust;
        Events.OnUnblock += UnblockSpeedAdjust;
        Events.OnDeath += Death;
    }

    private void OnDisable()
    {
        Events.OnBlock -= BlockSpeedAdjust;
        Events.OnUnblock -= UnblockSpeedAdjust;
        Events.OnDeath -= Death;
    }

    private void Death(InstanceId id)
    {
        if (transform.GetInstanceID() != id.Value) return;

        _isDead = true;
        if (_body != null)
        {
            _body.bodyType = RigidbodyType2D.Static;
        }

        _speed = _speedDefault;
        gameObject.layer = Layers.Dead;
    }

    private void BlockSpeedAdjust(InstanceId id)
    {
        if (transform.GetInstanceID() != id.Value) return;
        _speed = _slowWalkSpeed;
    }

    private void UnblockSpeedAdjust(InstanceId id)
    {
        if (transform.GetInstanceID() != id.Value) return;
        _speed = _speedDefault;
    }

    private void FixedUpdate()
    {
        if (_isDead || _body == null) return;

        FixedMove(Input2D);
    }

    private void Update()
    {
        if (_isDead || _body != null) return;

        Move(Input2D);
    }

    public void Move(Vector2 v)
    {
        var movement = v * _speed * Time.deltaTime;
        transform.position = transform.position + movement.WithZ();
    }

    public void FixedMove(Vector2 v)
    {
        var velocity = v * _speed;
        _body.velocity = velocity;
    }
}
