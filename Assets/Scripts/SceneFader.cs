using System;
using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class SceneFader : MonoBehaviour
{
    [SerializeField] private Image _fader;
    [SerializeField] private bool _fadeInAwake;

    private IEnumerator _coroutine;

    private void Awake()
    {
        if (_fadeInAwake)
        {
            FadeToAlpha();
        }
    }

    private void OnEnable()
    {
        Events.OnFadeOut += FadeOut;
        Events.OnFadeIn += FadeIn;
    }

    private void OnDisable()
    {
        Events.OnFadeOut -= FadeOut;
        Events.OnFadeIn -= FadeIn;
    }

    private void FadeOut()
    {
        FadeToColor(() => Events.FadeOutComplete());
//        FadeToColor(() => { SceneManager.LoadScene(Scenes.RoundEndMenu, LoadSceneMode.Additive); });
    }

    private void FadeIn()
    {
        FadeToAlpha(() => Events.FadeInComplete());
    }

    public void FadeToColor(Action callback = null)
    {
        if (_coroutine != null)
            StopCoroutine(_coroutine);

        _coroutine = FadeCoroutine(Color.clear, Color.black, .4f, true, callback);
        StartCoroutine(_coroutine);
    }

    public void FadeToAlpha(Action callback = null)
    {
        if (_coroutine != null)
            StopCoroutine(_coroutine);

        _coroutine = FadeCoroutine(Color.black, Color.clear, .4f, false, callback);
        StartCoroutine(_coroutine);
    }

    private IEnumerator FadeCoroutine(Color from, Color to, float duration, bool enabledOnReturn = true, Action callback = null)
    {
        _fader.enabled = true;

        if (!_fader.color.ApproxRgba(to))
        {
            var endTime = Time.time + duration;
            while (Time.time < endTime)
            {
                var dt = 1 - (endTime - Time.time) / duration;
                _fader.color = Color.Lerp(from, to, dt);

                yield return WaitFor.EndOfFrame;
            }
        }

        _fader.color = to;
        _fader.enabled = enabledOnReturn;

        callback?.Invoke();
    }
}
