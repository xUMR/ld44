using System;
using System.Collections;
using UnityEngine;
using Random = UnityEngine.Random;

public sealed class AudioManager : MonoBehaviour
{
    private static AudioManager _instance;
    public static AudioManager Instance => _instance;

    [SerializeField] private int _poolSize = 4;
    [SerializeField] private Vector2 _pitchRange = new Vector2(.9f, 1.1f);

    private ComponentPool<AudioSource> _audioPool;

    private void Awake()
    {
        if (this.EnsureSingletonInitialized(ref _instance)) return;

        var poolGameObject = new GameObject("Pool");
        poolGameObject.transform.SetParent(transform);

        _audioPool = new ComponentPool<AudioSource>(poolGameObject, _poolSize);
    }

    private void Update()
    {
        _poolSize = _audioPool.Size;
    }

    public AudioSource Prepare(AudioClip clip)
    {
        var audioSource = _audioPool.Rent();
        audioSource.clip = clip;

        return audioSource;
    }

    public AudioSource Play(AudioClip clip)
    {
        var audioSource = Prepare(clip);

        StartCoroutine(PlayThenReturnCoroutine(audioSource));

        return audioSource;
    }

    public AudioSource PlayLoop(AudioClip clip)
    {
        var audioSource = Prepare(clip);
        audioSource.loop = true;
        audioSource.Play();

        return audioSource;
    }

    public AudioSource PlayFor(AudioClip clip, float duration, Vector2 pitchRange)
    {
        var audioSource = Prepare(clip);
        audioSource.pitch = pitchRange.Random();
        StartCoroutine(PlayThenReturnCoroutine(audioSource, duration));

        return audioSource;
    }

    public AudioSource PlayRandomizedPitch(AudioClip clip)
    {
        var audioSource = Prepare(clip);
        audioSource.pitch = _pitchRange.Random();
        StartCoroutine(PlayThenReturnCoroutine(audioSource));

        return audioSource;
    }

    private static void ResetAudio(AudioSource audioSource)
    {
        audioSource.Stop();
        audioSource.clip = null;
        audioSource.loop = false;
        audioSource.pitch = 1;
        audioSource.volume = 1;
        audioSource.time = 0;
    }

    public void Return(AudioSource audioSource)
    {
        ResetAudio(audioSource);
        _audioPool.Return(audioSource);
    }

    private IEnumerator PlayThenReturnCoroutine(AudioSource audioSource)
    {
        audioSource.Play();

        while (audioSource.isPlaying)
            yield return WaitFor.EndOfFrame;

        Return(audioSource);
    }

    private IEnumerator PlayThenReturnCoroutine(AudioSource audioSource, float duration)
    {
        var maxPlayTime = audioSource.clip.length - duration;
        var playAt = Random.Range(0, maxPlayTime);

        audioSource.time = playAt;
        audioSource.Play();

        var endTime = Time.time + duration;
        while (Time.time < endTime)
        {
            audioSource.volume -= Time.deltaTime / duration;
            yield return WaitFor.EndOfFrame;
        }

        audioSource.Stop();
        Return(audioSource);
    }
}
