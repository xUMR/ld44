using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParticleSystemManager : MonoBehaviour
{
    private static ParticleSystemManager _instance;

    public static ParticleSystemManager Instance => _instance;

    [SerializeField] private int _initialPoolSize = 8;
    [SerializeField] private GameObject[] _prefabs;

    private Dictionary<ParticleType, PrefabPool> _pools;
    private Dictionary<ParticleType, float> _lifetimes;

    private void Awake()
    {
        if (this.EnsureSingletonInitialized(ref _instance)) return;

        _pools = new Dictionary<ParticleType, PrefabPool>(_prefabs.Length);
        _lifetimes = new Dictionary<ParticleType, float>(_prefabs.Length);

        foreach (var prefab in _prefabs)
        {
            var poolGameObject = new GameObject($"Pool ({prefab.name})");
            poolGameObject.transform.SetParent(transform);

            var type = prefab.GetComponent<ParticleTypeData>().ParticleType;

            _pools[type] = new PrefabPool(prefab, poolGameObject.transform, _initialPoolSize);
            _lifetimes[type] = prefab.GetComponent<ParticleSystem>().main.startLifetime.constant;
        }
    }

    public ParticleSystem Rent(ParticleType type)
    {
        var ps = _pools[type].Rent().GetComponent<ParticleSystem>();
        StartCoroutine(ReturnToPoolCoroutine(ps, type));
        return ps;
    }

    public void Return(ParticleSystem ps, ParticleType type) => _pools[type].Return(ps.gameObject);

    private IEnumerator ReturnToPoolCoroutine(ParticleSystem ps, ParticleType type)
    {
        yield return WaitFor.Seconds(_lifetimes[type] + 1);

        ps.gameObject.SetActive(false);
        Return(ps, type);
    }
}
