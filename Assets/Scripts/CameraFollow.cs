using UnityEngine;

public class CameraFollow : MonoBehaviour
{
    [SerializeField] private Transform _target;
    [SerializeField] private Vector3 _offset;
    [SerializeField] private float _speed;
    [SerializeField] private bool _lookAtTarget;

    private Vector3 _velocity;

    private void LateUpdate()
    {
        var destination = _target.position + _offset;
        var source = transform.position;
        transform.position = Vector3.SmoothDamp(source, destination, ref _velocity, _speed * Time.deltaTime);

        if (_lookAtTarget)
            transform.LookAt(_target);
    }
}
