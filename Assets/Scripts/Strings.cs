public static class Strings
{
    public static string WalletAmount => $"<b>{ShopManager.Instance.Wallet.Currency}</b>";
    public static string TotalAmount => $"<b>{ShopManager.Instance.Wallet.TotalEarnings}</b>";

    public static readonly string BuyShield = "Buy New Shield";
    public static readonly string UpgradeShield = "Upgrade Shield";
}
