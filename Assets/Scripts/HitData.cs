using UnityEngine;

public struct HitData
{
    public readonly Vector2 Point;
    public readonly InstanceId Id;
    public readonly float Damage;

    public HitData(Vector2 point, InstanceId id, float damage)
    {
        Point = point;
        Id = id;
        Damage = damage;
    }

    public HitData(Vector2 point, Object obj, float damage) : this(point, obj.InstanceId(), damage) { }

    public HitData(RaycastHit2D hit, float damage) : this(hit.point, hit.transform.InstanceId(), damage) { }

    public HitData(RaycastHit2D hit, Damage damage) : this(hit.point, hit.transform.InstanceId(), damage.Amount) { }
}
