using System;
using UnityEngine;

public class LoopData : MonoBehaviour
{
    [SerializeField] private AudioClip _clip;
    [SerializeField] private float[] _intervals;
    [SerializeField] private int _index;

    private bool _requestTempoReset;
    private bool _readyToIncreaseTempo;
    private int _maxIndex;

    private AudioSource _audio;

    private void Start()
    {
        _audio = _audio == null ? AudioManager.Instance.Prepare(_clip) : _audio;
        _maxIndex = _intervals.Length - 1;
    }

    private void OnEnable()
    {
        Events.OnRoundStart += BeginPlaying;
        Events.OnRoundStart += IncreaseTempo;
        Events.OnGameOver += ResetTempo;
    }

    private void OnDisable()
    {
        Events.OnRoundStart -= BeginPlaying;
        Events.OnRoundStart -= IncreaseTempo;
        Events.OnGameOver -= ResetTempo;
    }

    private void BeginPlaying()
    {
        if (!_audio.isPlaying)
        {
            _audio.Play();
        }
    }

    private void ResetTempo()
    {
        _requestTempoReset = true;
    }

    private void IncreaseTempo()
    {
        // increase tempo every two rounds
        if (_readyToIncreaseTempo)
        {
//            print("increasing tempo");
            _index = Math.Min(_index + 1, _maxIndex);
            _readyToIncreaseTempo = false;
        }
        else
        {
            _readyToIncreaseTempo = true;
        }
    }

    private void Update()
    {
        if (_audio.time > _intervals[_index + 1])
        {
            if (_requestTempoReset)
            {
                _index = 0;
                _audio.time = _intervals[_index];
                _requestTempoReset = false;
                return;
            }
            _audio.time = _intervals[_index];
        }
    }
}
