using UnityEngine;

public class LookAtCursor : MonoBehaviour
{
    [SerializeField] private Camera _camera;
    [SerializeField] private Transform _transform;
    [SerializeField] private float _offsetAngleZ;
    [SerializeField] private float _speed = 1;

    private bool _isDead;

    private void Awake()
    {
        _camera = _camera == null ? Camera.main : _camera;
        _transform = _transform == null ? transform : _transform;
    }

    private void OnEnable()
    {
        Events.OnDeath += Death;
    }

    private void OnDisable()
    {
        Events.OnDeath -= Death;
    }

    private void Death(InstanceId id)
    {
        if (_transform.GetInstanceID() != id.Value) return;

        _isDead = true;
    }

    private void LateUpdate()
    {
        if (_isDead) return;

        var mousePosWorld = _camera.ScreenToWorldPoint(Input.mousePosition);

        var difference = mousePosWorld - _transform.position;
        var angleZ = Mathf.Atan2(difference.y, difference.x) * Mathf.Rad2Deg + _offsetAngleZ;
        var eulerAngles = _transform.rotation.eulerAngles.WithZ(angleZ);
        var targetRotation = Quaternion.Euler(eulerAngles);

        _transform.rotation = Quaternion.Lerp(_transform.rotation, targetRotation, Time.deltaTime * _speed);
    }
}
