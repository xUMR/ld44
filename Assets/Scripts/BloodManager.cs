using System.Collections;
using UnityEngine;

public class BloodManager : MonoBehaviour
{
    private static BloodManager _instance;

    public static BloodManager Instance => _instance;

    [SerializeField] private GameObject _prefab;
    [SerializeField] private ParticleSystem.MinMaxGradient _gradient;

    [SerializeField] private float _prefabLifetime = 60;
    [SerializeField] private float _disappearDuration = 5;
    private PrefabPool _pool;

    private void Awake()
    {
        if (this.EnsureSingletonInitialized(ref _instance)) return;

        var poolGameObject = new GameObject("Pool");
        poolGameObject.transform.SetParent(transform);

        _pool = new PrefabPool(_prefab, poolGameObject.transform, 32, InitObject);
    }

    private void InitObject(GameObject go)
    {
        go.SetActive(false);

        var sprite = go.GetComponent<SpriteRenderer>();
        sprite.color = Color.Lerp(_gradient.colorMin, _gradient.colorMax, Random.value);
    }

    public GameObject Rent()
    {
        var obj = _pool.Rent();
        StartCoroutine(ReturnToPoolCoroutine(obj, _prefabLifetime));

        return obj;
    }

    public void Return(GameObject obj) => _pool.Return(obj.gameObject);

    private IEnumerator ReturnToPoolCoroutine(GameObject obj, float duration)
    {
        yield return WaitFor.Seconds(duration);

        var sprite = obj.GetComponent<SpriteRenderer>();

        var endTime = Time.time + _disappearDuration;
        while (Time.time < endTime)
        {
            sprite.color = sprite.color.AddAlpha(-Time.deltaTime / _disappearDuration);

            yield return WaitFor.EndOfFrame;
        }

        obj.SetActive(false);
        sprite.color = sprite.color.WithAlpha(1);

        Return(obj);
    }
}
