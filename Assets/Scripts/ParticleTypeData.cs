using UnityEngine;

public class ParticleTypeData : MonoBehaviour
{
    [SerializeField] private ParticleType _particleType;

    public ParticleType ParticleType => _particleType;
}

public enum ParticleType
{
    Blood,
    Deflect,
    Block
}
