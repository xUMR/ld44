using System.Collections;
using UnityEngine;

public class EnemyAi : MonoBehaviour
{
    [SerializeField] private CircleCollider2D _detectRange;
    [SerializeField] private CircleCollider2D _blockRange;
    [SerializeField] private CircleCollider2D _attackRange;
    [SerializeField] private CircleCollider2D _personalSpace;

    [SerializeField] private Equipment _sword;
    [SerializeField] private Equipment _shield;

    [SerializeField] private float _attackInterval = 3;
    [SerializeField] private float _decisionInterval = .5f;
    [SerializeField] private float _lowStamina = 4;

    [SerializeField] private float _rotationSpeed;
    [SerializeField] private float _offsetAngleZ;

    [SerializeField] private Movement _movement;
    [SerializeField] private Stamina _stamina;

    private bool _canBlock;
    private Transform _lookAtTarget;
    private Transform _player;

    private float _lastAttackTime;
    private float _lastMovementTime;
    private Vector2 _direction;

    private IEnumerator _coroutine;

    private bool AttackAllowed => _attackInterval.TimePassedSince(_lastAttackTime);

    private bool InAttackRange => _attackRange.OverlapPoint(_player.position);
    private bool InBlockRange => _blockRange.OverlapPoint(_player.position);
    private bool IsStaminaLow => _stamina.Value < _lowStamina;

    private Vector2 Forward => transform.up;
    private Vector2 Back => -transform.up;

    public void RepairShield() => _shield.Repair();

    private void OnEnable()
    {
        _player = _player == null ? GameObject.FindGameObjectWithTag(Tags.Player).transform : _player;

        _coroutine.TryStop(this);
        _coroutine = ThinkCoroutine();
        StartCoroutine(_coroutine);

        Events.OnDeath += Death;
        Events.OnThrustPeak += ThrustPeak;
        Events.OnRoundEnd += RoundEnd;
    }

    private void OnDisable()
    {
        _coroutine.TryStop(this);
        Events.OnDeath -= Death;
        Events.OnThrustPeak -= ThrustPeak;
        Events.OnRoundEnd -= RoundEnd;
    }

//    public void SetTarget(Transform target)
//    {
//        _player = target;
//    }

    private void RoundEnd()
    {
        enabled = false;
    }

    private void ThrustPeak(InstanceId id)
    {
        if (transform.GetInstanceID() != id.Value) return;

        RaycastHit2D result;
        if (DuelHelper.CollisionCheck(_sword, out result))
        {
            if (result.transform.root.CompareTag(Tags.Player))
            {
                Events.CameraShake(result.normal);
            }
        }
    }

    private void Death(InstanceId id)
    {
        if (transform.GetInstanceID() != id.Value) return;

        enabled = false;
        _sword.GetComponent<SpriteRenderer>().sortingLayerID = SortingLayers.DeadId;
        _shield.GetComponent<SpriteRenderer>().sortingLayerID = SortingLayers.DeadId;
    }

    private IEnumerator ThinkCoroutine()
    {
        var wait = WaitFor.Seconds(_decisionInterval);

        while (true)
        {
            DecideAttackDefence();

            yield return wait;
        }
    }

    private void DecideAttackDefence()
    {
        if (IsStaminaLow)
        {
            TryUnblock();
            return;
        }

        if (AttackAllowed)
        {
            if (!InAttackRange) return;

            if (!_canBlock)
            {
                Unblock();
            }

            _lastAttackTime = Time.time;
            Events.Swing(transform.InstanceId());
        }
        else
        {
            var shouldBlock = InBlockRange;

            if (_canBlock && shouldBlock)
            {
                Block();
            }
            else if (!_canBlock && !shouldBlock)
            {
                Unblock();
            }
        }
    }

    private void Block()
    {
        Events.Block(transform.InstanceId());
        _canBlock = false;
    }

    private void TryUnblock()
    {
        if (!_canBlock)
        {
            Unblock();
        }
    }

    private void Unblock()
    {
        Events.Unblock(transform.InstanceId());
        _canBlock = true;
    }

    private void FixedUpdate()
    {
        var shouldDetect = _detectRange.OverlapPoint(_player.position);
        _lookAtTarget = shouldDetect ? _player : null;
    }

    private void Update()
    {
        if (_lookAtTarget == null) return;

        var difference = _lookAtTarget.position - transform.position;
        var angleZ = Mathf.Atan2(difference.y, difference.x) * Mathf.Rad2Deg + _offsetAngleZ;
        var eulerAngles = transform.rotation.eulerAngles.WithZ(angleZ);
        var targetRotation = Quaternion.Euler(eulerAngles);

        transform.rotation = Quaternion.Lerp(transform.rotation, targetRotation, Time.deltaTime * _rotationSpeed);

        var closeEnoughToPlayer = InAttackRange;
        var tooCloseToPlayer = _personalSpace.OverlapPoint(_player.position);

        if (_decisionInterval.TimePassedSince(_lastMovementTime))
        {
            _lastMovementTime = Time.time;
            _direction = Vector2.zero;
            if (IsStaminaLow)
                _direction = Back;
            else if (!closeEnoughToPlayer)
                _direction = Forward;
            else if (tooCloseToPlayer)
                _direction = Back;
        }

        _movement.Move(_direction);
    }
}
