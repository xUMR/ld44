using System.Collections;
using UnityEngine;

public class CameraShake : MonoBehaviour
{
    [SerializeField] private float _multiplier = 1;
    [SerializeField] private float _speed;
    [SerializeField] private float _duration = .2f;

    private Vector3 _target;
    private Vector3 _velocity;

    private void OnEnable()
    {
        _target = transform.position;
        Events.OnCameraShake += Shake;
    }

    private void OnDisable()
    {
        Events.OnCameraShake -= Shake;
    }

    private void Shake(Vector2 magnitude)
    {
        StartCoroutine(ShakeCoroutine(magnitude));
    }

    private IEnumerator ShakeCoroutine(Vector2 magnitude)
    {
        var v = magnitude * _multiplier;
        var previousPosition = transform.position;
        _target = transform.position + v.WithZ();

        yield return WaitFor.Seconds(_duration);

        _target = previousPosition;
    }

    private void LateUpdate()
    {
        var destination = _target;
        var source = transform.position;
        transform.position = Vector3.SmoothDamp(source, destination, ref _velocity, _speed * Time.deltaTime);
    }
}
