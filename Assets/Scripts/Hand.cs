using System;
using UnityEngine;

public class Hand : MonoBehaviour
{
    [SerializeField] private GameObject _equipmentPrefab;

    private Transform _root;
    private Equipment _equipment;
    private Stamina _stamina;
    private FighterConfig _fighterConfig;

    private bool _isBlocking;

    private bool RootIsPlayer => _root.CompareTag(Tags.Player);

    private void Awake()
    {
        _equipment = _equipmentPrefab.GetComponent<Equipment>();

        _root = transform.root;
        if (!RootIsPlayer) return;

        if (_equipmentPrefab == null)
            _equipmentPrefab = transform.GetChild(0).gameObject;

        _stamina = _root.GetComponent<Stamina>();
        _fighterConfig = _root.GetComponent<Fighter>().Config;
    }

    private void OnEnable()
    {
        Events.OnMouseInput += MouseInput;
        Events.OnStaminaDepleted += StaminaDepleted;
        Events.OnThrustPeak += ThrustPeak;
    }

    private void OnDisable()
    {
        Events.OnMouseInput -= MouseInput;
        Events.OnStaminaDepleted -= StaminaDepleted;
        Events.OnThrustPeak -= ThrustPeak;
    }

    private void StaminaDepleted(InstanceId obj)
    {
        Unblock();
    }

    private void ThrustPeak(InstanceId id)
    {
        if (!RootIsPlayer) return;
        if (_equipment.Type != EquipmentType.Sword) return;
        if (_root.GetInstanceID() != id.Value) return;

        RaycastHit2D result;
        if (DuelHelper.CollisionCheck(_equipment, out result))
        {
            Events.CameraShake(-result.normal);
        }
    }

    private void MouseInput(MouseButton btn)
    {
        if (!RootIsPlayer) return;

//        equipment.Weight
//        equipment.Speed
//        stamina.Value

        if (_stamina.Value < _equipment.StaminaDrain || Mathf.Approximately(_stamina.Value, 0))
        {
            Unblock();

            return;
        }

        if (_equipment.IsBroken) return;

        switch (_equipment.Type)
        {
            case EquipmentType.Shield:
                UseShield(btn);
                break;
            case EquipmentType.Sword:
                UseSword(btn);
                break;
            default:
                throw new ArgumentOutOfRangeException();
        }
    }

    private void UseSword(MouseButton btn)
    {
        if (btn != MouseButton.LeftDown) return;
        Events.Swing(_root.InstanceId());
    }

    private void UseShield(MouseButton btn)
    {
        switch (btn) {
            case MouseButton.RightDown:
                if (!_isBlocking)
                {
                    Events.Block(_root.InstanceId());
                    _isBlocking = true;
                }

                break;
            case MouseButton.RightUp:
                Unblock();
                break;
            case  MouseButton.Both:
                print("bash");
                StartCoroutine(BashCoroutine());
                break;
            default:
                return;
        }
    }

    private System.Collections.IEnumerator BashCoroutine()
    {
        var body = _root.GetComponent<Rigidbody2D>();

        var duration = .15f;
        var movement = _root.up * 2;
        var dm = movement / duration;
        var endTime = Time.time + duration;
        while (Time.time < endTime)
        {
            body.MovePosition(_root.position + dm * Time.deltaTime);
            yield return WaitFor.EndOfFrame;
        }
        body.velocity = Vector2.zero;
    }

    private void Unblock()
    {
        if (_isBlocking)
        {
            Events.Unblock(_root.InstanceId());
            _isBlocking = false;
        }
    }

    private void LateUpdate()
    {
//        if (!RootIsPlayer) return;
        if (_equipment.Type != EquipmentType.Sword) return;

//        Debug.DrawLine(SwingStartPosition(), SwingEndPosition(), Color.cyan);
        Debug.DrawLine(DuelHelper.ThrustStartPosition(_root), DuelHelper.ThrustEndPosition(_root), Color.blue);
    }
}
