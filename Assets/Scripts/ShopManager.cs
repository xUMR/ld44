using UnityEngine;

public class ShopManager : MonoBehaviour
{
    private static ShopManager _instance;

    public static ShopManager Instance => _instance;

    [SerializeField] private Wallet _wallet;

    public Wallet Wallet => _wallet;

    private void Awake()
    {
        if (this.EnsureSingletonInitialized(ref _instance)) return;
    }

    private void OnEnable()
    {
        Events.OnRestartGame += RestartGame;
    }

    private void OnDisable()
    {
        Events.OnRestartGame -= RestartGame;
    }

    private void RestartGame()
    {
        _wallet = GameObject.FindObjectOfType<Wallet>();
    }
}
