using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    private static GameManager _instance;

    public static GameManager Instance => _instance;

    public GameState GameState;

    private void Awake()
    {
        if (this.EnsureSingletonInitialized(ref _instance)) return;

        Events.OnGameOver += GameOver;
        Events.OnRoundEnd += EndRound;
        Events.OnFadeOutComplete += FadeOutComplete;
        Events.OnFadeInComplete += FadeInComplete;
    }

    private void OnDisable()
    {
        Events.OnGameOver -= GameOver;
        Events.OnRoundEnd -= EndRound;
        Events.OnFadeOutComplete -= FadeOutComplete;
        Events.OnFadeInComplete -= FadeInComplete;
    }

    private void GameOver()
    {
        GameState = GameState.GameOver;
        Events.FadeOut();
    }

    private void EndRound()
    {
        GameState = GameState.EndRound;
        Events.FadeOut();
    }

    private void FadeInComplete()
    {
        if (GameState == GameState.Playing)
        {
            Events.RoundStart();
        }
    }

    private void FadeOutComplete()
    {
        if (GameState == GameState.GameOver)
        {
            SceneManager.LoadScene(Scenes.GameOverMenu, LoadSceneMode.Additive);
        }
        else if (GameState == GameState.EndRound)
        {
            SceneManager.LoadScene(Scenes.RoundEndMenu, LoadSceneMode.Additive);
        }
    }

    private void Start()
    {
//        Events.RoundStart();
        Events.FadeIn();
    }
}

public enum GameState
{
    Playing,
    EndRound,
    GameOver
}
