using System.Collections.Generic;
using UnityEngine;

public static class WaitFor
{
    private static readonly Dictionary<float, WaitForSeconds> Waiters = new Dictionary<float, WaitForSeconds>();

    public static readonly WaitForEndOfFrame EndOfFrame = new WaitForEndOfFrame();

    public static WaitForSeconds Seconds(float seconds)
    {
        WaitForSeconds wait;
        if (!Waiters.TryGetValue(seconds, out wait))
        {
            wait = new WaitForSeconds(seconds);
            Waiters[seconds] = wait;
        }

        return wait;
    }
}
