﻿using UnityEngine;

public class EmptySingleton : MonoBehaviour
{
    private EmptySingleton _instance;
    public EmptySingleton Instance => _instance;

    private void Awake()
    {
        this.EnsureSingletonInitialized(ref _instance);
    }
}
