using UnityEngine;
using UnityEngine.SceneManagement;

public class RoundEndMenu : MonoBehaviour
{
    [SerializeField] private SceneFader _sceneFader;

    public void Continue()
    {
        _sceneFader.FadeToColor(() => LoadNextScene());
    }

    private static void LoadNextScene()
    {
        SceneManager.UnloadSceneAsync(Scenes.RoundEndMenu)
            .completed += _ => { SceneManager.LoadScene(Scenes.ShopMenu, LoadSceneMode.Additive); };
    }

    private void Update()
    {
        if (Input.GetAxisRaw("Submit") > 0)
        {
            Continue();
        }
    }
}
