using UnityEngine;

public static class DuelHelper
{
    private static int _thrustCounter;
    private static int _hitCounter;
    private static int _cutCounter;
    private static int _blockCounter;
    private static int _parryCounter;

    public static bool CollisionCheck(Equipment equipment, out RaycastHit2D hit)
    {
        var root = equipment.transform.root;

        _thrustCounter++;

        var tempLayer = equipment.gameObject.layer;
        equipment.gameObject.layer = Layers.IgnoreRaycast;

//        var mask = Layers.DefenceActiveMask | Layers.FleshMask | Layers.DamageActiveMask;
        var mask = equipment.DamageLayer.Value;
        hit = Physics2D.Linecast(ThrustStartPosition(root), ThrustEndPosition(root), mask);
        equipment.gameObject.layer = tempLayer;
        if (hit.transform == null) return false;

        _hitCounter++;

        var resultLayer = hit.transform.gameObject.layer;
        if (resultLayer == Layers.Flesh)
        {
            _cutCounter++;
            var damage = equipment.GetComponent<Damage>();
            var hitData = new HitData(hit, damage);

            Events.Hit(hitData);
            Events.SuccessfulCut();
        }
        else if (resultLayer == Layers.DefenceActive)
        {
            _blockCounter++;
            // block
            var damage = equipment.GetComponent<Damage>();

            var shield = hit.transform.GetComponent<Equipment>();
            var targetShieldWeight = shield.Weight;
            shield.DamageEquipment(Mathf.CeilToInt(damage.Amount));

            var data = new StaminaDrainData(hit.transform.root.InstanceId(), targetShieldWeight);
            Events.DrainStamina(data);

            var effect = ParticleSystemManager.Instance.Rent(ParticleType.Block);
            effect.transform.position = hit.point;
            effect.gameObject.SetActive(true);

            effect.Play();

            Events.SuccessfulBlock();
        }
        else if (resultLayer == Layers.DamageActive)
        {
            _parryCounter++;
            // parry
            var data = new StaminaDrainData(hit.transform.root.InstanceId(), equipment.StaminaDrain * .1f);
            Events.DrainStamina(data);

            var effect = ParticleSystemManager.Instance.Rent(ParticleType.Deflect);
            effect.transform.position = hit.point;
            effect.gameObject.SetActive(true);

            effect.Play();

            Events.SuccessfulParry();
        }
        else
        {
            return false;
        }

//        Debug.Log($"Time: {Time.time}\nThrusts: {_thrustCounter}\nHits: {_hitCounter}\nCuts: {_cutCounter}\nBlocks: {_blockCounter}\nParries: {_parryCounter}");

        return true;
    }

    public static Vector2 ThrustStartPosition(Transform root)
    {
        return (root.position + new Vector3(.25f, .5f))
            .RotateAround(root.position, root.rotation.eulerAngles.z);
    }

    public static Vector2 ThrustEndPosition(Transform root)
    {
        return (root.position + new Vector3(0, 2.5f))
            .RotateAround(root.position, root.rotation.eulerAngles.z);
    }

//    private static Vector2 SwingStartPosition()
//    {
//        return (_root.position + new Vector3(1.5f, 2.25f))
//            .RotateAround(_root.position, _root.rotation.eulerAngles.z);
//    }
//
//    private static Vector2 SwingEndPosition()
//    {
//        return (_root.position + new Vector3(-1, 2))
//            .RotateAround(_root.position, _root.rotation.eulerAngles.z);
//    }
}
