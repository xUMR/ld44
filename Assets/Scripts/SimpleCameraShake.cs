using System.Collections;
using UnityEngine;

public class SimpleCameraShake : MonoBehaviour
{
    [SerializeField] private float _multiplier = 1;

    private void OnEnable()
    {
        Events.OnCameraShake += Shake;
    }

    private void OnDisable()
    {
        Events.OnCameraShake -= Shake;
    }

    private void Shake(Vector2 magnitude) => StartCoroutine(ShakeCoroutine(magnitude));

    private IEnumerator ShakeCoroutine(Vector2 magnitude)
    {
        var offset = _multiplier * new Vector3(magnitude.x, magnitude.y);
        transform.position += offset;
        yield return WaitFor.Seconds(.1f);
        transform.position -= offset;
    }
}
