using System;
using System.Collections.Generic;
using UnityEngine;
using Object = UnityEngine.Object;

public sealed class PrefabPool
{
    private readonly Transform _parent;
    private readonly GameObject _prefab;
    private readonly List<GameObject> _availableItems;

    public int Size { get; private set; }

    public event Action<GameObject> OnBeforeRent = _ => { };
    public event Action<GameObject> OnBeforeReturn = _ => { };

    public PrefabPool(GameObject prefab, Transform parent, int initialSize, Action<GameObject> initializer = null)
    {
        _prefab = prefab;
        _parent = parent;
        _availableItems = new List<GameObject>(initialSize);
        Size = initialSize;

        for (var i = 0; i < initialSize; i++)
        {
            var obj = Object.Instantiate(_prefab, _parent);
            initializer?.Invoke(obj);

            _availableItems.Add(obj);
        }
    }

    public GameObject Rent()
    {
        GameObject obj;
        if (_availableItems.Count == 0)
        {
            obj = Object.Instantiate(_prefab, _parent);
            Size++;
        }
        else
        {
            obj = _availableItems.RemoveLast();
        }

        OnBeforeRent(obj);
        return obj;
    }

    public void Return(GameObject obj)
    {
        OnBeforeReturn(obj);
        _availableItems.Add(obj);
    }

    public void Clear()
    {
        _availableItems.Clear();
    }
}
