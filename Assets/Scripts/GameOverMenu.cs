using UnityEngine;
using UnityEngine.SceneManagement;

public class GameOverMenu : MonoBehaviour
{
    [SerializeField] private SceneFader _fader;

    public void ReturnToMainMenu()
    {
        _fader.FadeToColor(() => SceneManager.LoadScene(Scenes.MainMenu));
    }
}
