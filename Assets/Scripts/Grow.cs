using System.Collections;
using UnityEngine;

public class Grow : MonoBehaviour
{
    [SerializeField] private float _duration = 1;
    [SerializeField] private ParticleSystem.MinMaxCurve _growthMultiplier = new ParticleSystem.MinMaxCurve(1);
    [SerializeField] private AnimationCurve _curve;

    public float Growth { get; set; } = 0;

    public ParticleSystem.MinMaxCurve GrowthMultiplier => _growthMultiplier;

    private IEnumerator _coroutine;

    private void OnEnable()
    {
        if (_coroutine != null)
            StopCoroutine(_coroutine);

        _coroutine = GrowCoroutine();
        StartCoroutine(GrowCoroutine());
    }

    private void OnDisable()
    {
        if (_coroutine != null)
            StopCoroutine(_coroutine);
    }

    private IEnumerator GrowCoroutine()
    {
        var growth = Mathf.Approximately(Growth, 0)
            ? _growthMultiplier.Evaluate(Random.value)
            : Growth;

        var startTime = Time.time;
        var endTime = Time.time + _duration;
        while (Time.time < endTime)
        {
            var dt = Time.time - startTime;
            var size = _curve.Evaluate(dt);

            transform.localScale = Vector3.one * size * growth;

            yield return WaitFor.EndOfFrame;
        }
    }
}
