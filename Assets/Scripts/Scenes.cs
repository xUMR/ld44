public static class Scenes
{
    public static readonly string RoundEndMenu = "RoundEndMenu";
    public static readonly string ShopMenu = "ShopMenu";
    public static readonly string Test = "Test";
    public static readonly string GameOverMenu = "GameOverMenu";
    public static readonly string MainMenu = "MainMenu";
}
