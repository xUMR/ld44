using System.Collections;
using UnityEngine;

public class CanvasGroupFader : MonoBehaviour
{
    private CanvasGroup _canvas;

    private IEnumerator _coroutine;

    private void Awake()
    {
        _canvas = GetComponent<CanvasGroup>();
    }

    public void Fade(float target, float duration = 0)
    {
        _coroutine.TryStop(this);

        _coroutine = FadeCoroutine(target, duration);
        StartCoroutine(_coroutine);
    }

    private IEnumerator FadeCoroutine(float target, float duration = 0)
    {
        var endTime = Time.time + duration;
        var d = 1 / duration;
        while (Time.time < endTime)
        {
            _canvas.alpha += d * Time.deltaTime;
            yield return WaitFor.EndOfFrame;
        }

        _canvas.alpha = target;
    }
}
