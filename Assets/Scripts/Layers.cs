using UnityEngine;

public static class Layers
{
    public static readonly int Default = LayerMask.NameToLayer("Default");
    public static readonly int DamageActive = LayerMask.NameToLayer("DamageActive");
    public static readonly int DamagePassive = LayerMask.NameToLayer("DamagePassive");
    public static readonly int DefenceActive = LayerMask.NameToLayer("DefenceActive");
    public static readonly int DefencePassive = LayerMask.NameToLayer("DefencePassive");
    public static readonly int Flesh = LayerMask.NameToLayer("Flesh");
    public static readonly int IgnoreRaycast = LayerMask.NameToLayer("Ignore Raycast");
    public static readonly int Dead = LayerMask.NameToLayer("Dead");
    public static readonly int Block = LayerMask.NameToLayer("Block");

    public static readonly int DamageActiveMask = 1 << DamageActive;
    public static readonly int DamagePassiveMask = 1 << DamagePassive;
    public static readonly int DefenceActiveMask = 1 << DefenceActive;
    public static readonly int DefencePassiveMask = 1 << DefencePassive;
    public static readonly int FleshMask = 1 << Flesh;
    public static readonly int IgnoreRaycastMask = 1 << IgnoreRaycast;
}
