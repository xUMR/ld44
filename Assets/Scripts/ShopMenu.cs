using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class ShopMenu : MonoBehaviour
{
    [SerializeField] private SceneFader _sceneFader;
    [SerializeField] private Text _currencyText;

    private void OnEnable()
    {
        Events.OnPurchase += Purchase;
    }

    private void OnDisable()
    {
        Events.OnPurchase -= Purchase;
    }

    private void Purchase(PurchasableType type, float cost)
    {
        _currencyText.text = Strings.WalletAmount;
    }

    public void Continue()
    {
        _sceneFader.FadeToColor(() => LoadNextScene());
    }

    private static void LoadNextScene()
    {
        SceneManager.UnloadSceneAsync(Scenes.ShopMenu)
            .completed += _ =>
        {
            GameManager.Instance.GameState = GameState.Playing;
            Events.FadeIn();
        };
    }

    private void Update()
    {
        if (Input.GetAxisRaw("Submit") > 0)
        {
            Continue();
        }
    }
}
