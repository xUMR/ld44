using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class FighterSpawner : MonoBehaviour
{
    private static FighterSpawner _instance;

    public static FighterSpawner Instance => _instance;

    [SerializeField] private int _numberLeft;
    [SerializeField] private int _currentLevel;
    [SerializeField] private int _initialPoolSize = 4;

    private int _maxLevel;

    [SerializeField] private GameObject[] _prefabs;
    private Dictionary<int, PrefabPool> _pools;
    private Queue<GameObject> _rentedFighters;

    private Transform _player;

    private void Awake()
    {
        if (this.EnsureSingletonInitialized(ref _instance)) return;

        _pools = new Dictionary<int, PrefabPool>(_prefabs.Length);
        _rentedFighters = new Queue<GameObject>(_initialPoolSize * _prefabs.Length);

        PopulatePools();

        _maxLevel = _prefabs.Last().GetComponent<Fighter>().Level;

        Events.OnRoundStart += RoundStart;
        Events.OnFadeOutComplete += FadeOutComplete;
        Events.OnDeath += Death;
        Events.OnRestartGame += RestartGame;
        Events.OnGameOver += GameOver;
    }

    private void OnDisable()
    {
        Events.OnRoundStart -= RoundStart;
        Events.OnFadeOutComplete -= FadeOutComplete;
        Events.OnDeath -= Death;
        Events.OnRestartGame -= RestartGame;
        Events.OnGameOver -= GameOver;
    }

    private void GameOver()
    {
        foreach (var pair in _pools)
        {
            pair.Value.Clear();
        }
    }

    private void RestartGame()
    {
        _numberLeft = 2;
        _currentLevel = 0;
        FetchPlayer();
        PopulatePools();
    }

    private void PopulatePools()
    {
        foreach (var prefab in _prefabs)
        {
            var poolGameObject = new GameObject($"Pool ({prefab.name})");
            poolGameObject.transform.SetParent(transform);

            var level = prefab.GetComponent<Fighter>().Level;
            _pools[level] = new PrefabPool(prefab, null, _initialPoolSize, InitializeFighter);
        }
    }

    private void FadeOutComplete()
    {
        foreach (var fighter in _rentedFighters)
        {
            ResetFighter(fighter);
            Return(fighter.GetComponent<Fighter>());
            _numberLeft++;
        }
        _rentedFighters.Clear();

        if (_numberLeft - _currentLevel > 2)
        {
            _currentLevel = Mathf.Min(_maxLevel, _currentLevel + 1);
            _numberLeft--;
        }
        else
        {
            _numberLeft++;
        }
    }

    private void ResetFighter(GameObject fighter)
    {
        var ai = fighter.GetComponent<EnemyAi>();
        ai.RepairShield();
        fighter.GetComponent<Health>().Heal();
        ai.enabled = true;
        fighter.layer = Layers.Default;

        fighter.SetActive(false);
    }

    private void InitializeFighter(GameObject go)
    {
        go.SetActive(false);
    }

    private void RoundStart()
    {
        FetchPlayer();
        Spawn(_currentLevel);
    }

    private void FetchPlayer()
    {
        _player = _player == null ? GameObject.FindWithTag(Tags.Player).transform : _player;
    }

    public void Spawn(int level)
    {
        _numberLeft--;

        var go = _pools[level].Rent();

        var x = _player.position.x > 0 ? -7 : 7;
        var y = _player.position.y > 0 ? -4 : 4f;

        go.transform.position = new Vector3(x, y);
        go.SetActive(true);

        _rentedFighters.Enqueue(go);
    }

    public void Return(Fighter fighter)
    {
        _pools[fighter.Level].Return(fighter.gameObject);
    }

    private void Death(InstanceId id)
    {
        if (_player.GetInstanceID() == id.Value) return;

        if (_numberLeft == 0)
        {
            Events.RoundEnd();
            return;
        }

        Spawn(_currentLevel);
    }

//    private IEnumerator ReturnCoroutine(InstanceId id)
//    {
//        yield break;
//    }
}
