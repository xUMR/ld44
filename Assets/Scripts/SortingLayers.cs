using UnityEngine;

public static class SortingLayers
{
    public static readonly int BackgroundId = SortingLayer.NameToID("Background");
    public static readonly int DeadId = SortingLayer.NameToID("Dead");
    public static readonly int DefaultId = SortingLayer.NameToID("Default");
    public static readonly int ForegroundId = SortingLayer.NameToID("Foreground");
}
