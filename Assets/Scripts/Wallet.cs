using UnityEngine;

public class Wallet : MonoBehaviour
{
    [SerializeField] private decimal _totalEarnings;
    [SerializeField] private decimal _currency;
    [SerializeField] private float _multiplier = .2f;

    public decimal Currency => _currency;
    public decimal TotalEarnings => _totalEarnings;

    private void OnEnable()
    {
        Events.OnAddMoney += AddMoney;
        Events.OnPurchase += Purchase;
    }

    private void OnDisable()
    {
        Events.OnAddMoney -= AddMoney;
        Events.OnPurchase -= Purchase;
    }

    private void AddMoney(float amount)
    {
        var earned = new decimal(_multiplier * amount);
        _currency += earned;
        _totalEarnings += earned;
    }

    private void Purchase(PurchasableType type, float cost)
    {
        _currency -= new decimal(cost);
    }
}
