using UnityEngine;

public class AudioEvents : MonoBehaviour
{
    [SerializeField] private AudioClip[] _blockClips;
    [SerializeField] private AudioClip[] _cutClips;
    [SerializeField] private AudioClip[] _parryClips;
    [SerializeField] private AudioClip[] _spillClips;

    private void OnEnable()
    {
        Events.OnSuccessfulBlock += Block;
        Events.OnSuccessfulCut += Cut;
        Events.OnSuccessfulCut += Spill;
        Events.OnSuccessfulParry += Parry;
    }

    private void OnDisable()
    {
        Events.OnSuccessfulBlock -= Block;
        Events.OnSuccessfulCut -= Cut;
        Events.OnSuccessfulCut -= Spill;
        Events.OnSuccessfulParry -= Parry;
    }

    private void Block() => PlayRandomized(_blockClips.Random());
    private void Cut() => PlayRandomized(_cutClips.Random());
    private void Spill() => PlayRandomized(_spillClips.Random());
    private void Parry() => PlayRandomized(_parryClips.Random());

    private static void PlayRandomized(AudioClip clip) => AudioManager.Instance.PlayRandomizedPitch(clip);
}
