using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Health : MonoBehaviour
{
    [SerializeField] private float _value;
    [SerializeField] private Slider _slider;
    [SerializeField] private Transform[] _bodyParts;
    [SerializeField] private float[] _damageMultipliers;

    private float _defaultValue;

    private List<InstanceId> _bodyPartsIds;
    private Dictionary<InstanceId, float> _bodyPartDamageMultiplier;
    private Dictionary<InstanceId, SpriteRenderer> _bodyPartSprites;
    private Dictionary<InstanceId, Color> _bodyPartSpriteColors;

    public bool IsWounded => !_value.Approx(_defaultValue);

    private void Awake()
    {
        _defaultValue = _value;

        _bodyPartsIds = new List<InstanceId>();
        _bodyPartDamageMultiplier = new Dictionary<InstanceId, float>(_bodyParts.Length);
        _bodyPartSprites = new Dictionary<InstanceId, SpriteRenderer>(_bodyParts.Length);
        _bodyPartSpriteColors = new Dictionary<InstanceId, Color>(_bodyParts.Length);

        for (var i = 0; i < _bodyParts.Length; i++)
        {
            var bodyPart = _bodyParts[i];
            var id = bodyPart.InstanceId();
            _bodyPartsIds.Add(id);

            var sprite = bodyPart.GetComponent<SpriteRenderer>();
            _bodyPartSprites[id] = sprite;
            _bodyPartDamageMultiplier[id] = _damageMultipliers[i];
            _bodyPartSpriteColors[id] = sprite.color;
        }
    }

    private void OnEnable()
    {
        Events.OnHit += Hit;
        Events.OnFadeOutComplete += FadeOutComplete;
        Events.OnPurchase += Purchase;
        Events.OnDeath += Death;
    }

    private void OnDisable()
    {
        Events.OnHit -= Hit;
        Events.OnFadeOutComplete -= FadeOutComplete;
        Events.OnPurchase -= Purchase;
        Events.OnDeath -= Death;
    }

    private void Purchase(PurchasableType type, float cost)
    {
        if (type == PurchasableType.Heal)
        {
            Heal();
        }
    }

    private void Death(InstanceId id)
    {
        if (transform.GetInstanceID() != id.Value) return;

        DarkenColors();
        Events.AddMoney(_defaultValue - _value);
    }

    private void FadeOutComplete()
    {
        if (gameObject.CompareTag(Tags.Player)) return;

        if (GameManager.Instance.GameState == GameState.EndRound)
        {
            SetBodyPartLayers(Layers.Flesh, SortingLayers.DefaultId);
            ResetColors();
        }
    }

    private void ResetColors()
    {
        foreach (var id in _bodyPartsIds)
        {
            _bodyPartSprites[id].color = _bodyPartSpriteColors[id];
        }
    }

    private void DarkenColors()
    {
        if (gameObject.CompareTag(Tags.Player)) return;

        foreach (var id in _bodyPartsIds)
        {
            _bodyPartSprites[id].color = (_bodyPartSprites[id].color * .85f).WithAlpha(1);
        }
    }

    private void Hit(HitData hit)
    {
        float multiplier;
        if (!_bodyPartDamageMultiplier.TryGetValue(hit.Id, out multiplier)) return;

//        StartCoroutine(BleedCoroutine(hit, multiplier));
        Bleed(hit, multiplier);

        var actualDamage = hit.Damage * multiplier;
        _value -= actualDamage;
        if (_value <= 0)
        {
            _value = 0;
            SetBodyPartLayers(Layers.Default, SortingLayers.DeadId);

            Events.Death(transform.InstanceId());
        }
    }

    public void SetBodyPartLayers(int layerId, int sortingLayerId)
    {
        foreach (var bodyPart in _bodyParts)
        {
            bodyPart.gameObject.layer = layerId;
            var id = new InstanceId(bodyPart.transform.GetInstanceID());
            _bodyPartSprites[id].sortingLayerID = sortingLayerId;
        }
    }

    private void Bleed(HitData hit, float multiplier)
    {
        var bloodParticles = ParticleSystemManager.Instance.Rent(ParticleType.Blood);
        bloodParticles.transform.position = hit.Point;

        var shape = bloodParticles.shape;
        var offsetAngleZ = .5f.Roll(90, -90);
        shape.rotation = transform.rotation.eulerAngles.AddZ(offsetAngleZ);

        bloodParticles.gameObject.SetActive(true);
        bloodParticles.Play();

        var go = BloodManager.Instance.Rent();
        var grow = go.GetComponent<Grow>();
        var range = grow.GrowthMultiplier;
        grow.Growth = (multiplier / 2).Scale(range.constantMin, range.constantMax);

        go.transform.position = hit.Point;
        go.SetActive(true);

//        var sprite = _bodyPartSprites[hit.Id];
//        var damage = hit.Damage * multiplier;
//        const float duration = 1f;
//        var endTime = Time.time + duration;
//        var deltaRed = damage / duration * .05f;
//        while (Time.time < endTime)
//        {
//            sprite.color = sprite.color.AddRgb(deltaRed * Time.deltaTime);
//            yield return WaitFor.EndOfFrame;
//        }
//        sprite.color = sprite.color.AddRgb(damage * .05f);
    }

    public void Heal() => _value = _defaultValue;

    private void Update()
    {
        if (_slider == null) return;

        _slider.value = Mathf.Lerp(_slider.value, _value / _defaultValue, 2 * Time.deltaTime);
    }
}
