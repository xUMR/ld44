using System;
using System.Collections.Generic;
using UnityEngine;

public class ComponentPool<TComponent> where TComponent : Component
{
    private readonly GameObject _gameObject;
    private readonly List<TComponent> _availableItems;

    public int Size { get; private set; }

    public event Action<TComponent> OnBeforeRent = c => { };
    public event Action<TComponent> OnBeforeReturn = c => { };

    public ComponentPool(GameObject gameObject, int initialSize, Action<TComponent> initialize = null)
    {
        _gameObject = gameObject;
        _availableItems = new List<TComponent>(initialSize);
        Size = initialSize;

        for (var i = 0; i < initialSize; i++)
        {
            var component = _gameObject.AddComponent<TComponent>();
            initialize?.Invoke(component);

            _availableItems.Add(component);
        }
    }

    public TComponent Rent()
    {
        TComponent component;
        if (_availableItems.Count == 0)
        {
            component = _gameObject.AddComponent<TComponent>();
            Size++;
        }
        else
        {
            component = _availableItems.RemoveLast();
        }

        OnBeforeRent(component);
        return component;
    }

    public void Return(TComponent component)
    {
        OnBeforeReturn(component);
        _availableItems.Add(component);
    }
}
