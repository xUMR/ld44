using UnityEngine;
using UnityEngine.UI;

public class Stamina : MonoBehaviour
{
    [SerializeField] private float _value = 10;
//    [SerializeField] private float _perHit = 1;
    [SerializeField] private float _regen = 2;
    [SerializeField] private float _regenDelay = .7f;
    [SerializeField] private Slider _slider;

    private float _maxValue;
    private float _regenInitial;
    private float _lastActionTime;

    public float Value => _value;

    private void Awake()
    {
        _maxValue = _value;
        _regenInitial = _regen;
    }

    private void OnEnable()
    {
        Events.OnSwing += Swing;
        Events.OnBlock += Block;
        Events.OnUnblock += Unblock;
        Events.OnDrainStamina += DrainStamina;
    }

    private void OnDisable()
    {
        Events.OnSwing -= Swing;
        Events.OnBlock -= Block;
        Events.OnUnblock -= Unblock;
        Events.OnDrainStamina -= DrainStamina;
    }

    private void DrainStamina(StaminaDrainData data)
    {
        if (transform.InstanceId() != data.Id) return;

        _value = Mathf.Max(0, _value - data.Amount);
        if (_value.IsZero())
        {
            Events.StaminaDepleted(data.Id);
        }

        UpdateLastActionTime();
    }

    private void Block(InstanceId id)
    {
        if (transform.GetInstanceID() != id.Value) return;

//        UpdateLastActionTime();
        _regen = 0;
    }

    private void Unblock(InstanceId id)
    {
        if (transform.GetInstanceID() != id.Value) return;

        UpdateLastActionTime();
        _regen = _regenInitial;
    }

    private void Swing(InstanceId id)
    {
        if (transform.GetInstanceID() != id.Value) return;

//        UpdateLastActionTime();
    }

    private void Update()
    {
        if (_lastActionTime + _regenDelay > Time.time) return;

        _value = Mathf.Min(_maxValue, _value + _regen * Time.deltaTime);
    }

    private void LateUpdate()
    {
        UpdateSlider();
    }

    private void UpdateLastActionTime() => _lastActionTime = Time.time;

    private void UpdateSlider()
    {
        if (_slider == null) return;

        _slider.value = _value / _maxValue;
    }
}
