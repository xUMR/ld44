using System;

public struct InstanceId : IEquatable<InstanceId>
{
    public readonly int Value;

    public InstanceId(int instanceId)
    {
        Value = instanceId;
    }

    public InstanceId(UnityEngine.Object obj) : this(obj.GetInstanceID()) { }

    public static bool operator ==(InstanceId id1, InstanceId id2) => id1.Value == id2.Value;
    public static bool operator !=(InstanceId id1, InstanceId id2) => id1.Value != id2.Value;

    public bool Equals(InstanceId other) => Value == other.Value;

    public override bool Equals(object obj)
    {
        if (ReferenceEquals(null, obj)) return false;
        return obj is InstanceId && Equals((InstanceId) obj);
    }

    public override int GetHashCode() => Value.GetHashCode();
}
