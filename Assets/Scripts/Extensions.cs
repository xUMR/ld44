using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Object = UnityEngine.Object;
using Rng = UnityEngine.Random;

public static class Extensions
{
    // todo remove unused methods

    public static bool IsNonPositive(this float f) => f < float.Epsilon;
    public static bool IsNonNegative(this float f) => f > -float.Epsilon;
    public static bool IsZero(this float f) => Math.Abs(f) < float.Epsilon;
    public static bool IsZero(this float f, float epsilon) => Math.Abs(f) < epsilon;
    public static bool IsNonZero(this float f) => !IsZero(f);

    public static bool Approx(this float f, float other) => IsZero(f - other);
    public static bool Approx(this float f, float other, float epsilon) => IsZero(f - other, epsilon);
    public static bool LessOrApprox(this float f, float other) => Approx(f, other) || f < other;
    public static bool GreaterOrApprox(this float f, float other) => Approx(f, other) || f > other;

    public static bool Approx(this Vector2 v, Vector2 t) => v.x.Approx(t.x) && v.y.Approx(t.y);

    public static float Random(this Vector2 v) => Rng.Range(v.x, v.y);

    public static Vector3 AddX(this Vector3 v, float x) => new Vector3(v.x + x, v.y, v.z);
    public static Vector3 AddY(this Vector3 v, float y) => new Vector3(v.x, v.y + y, v.z);
    public static Vector3 AddZ(this Vector3 v, float z) => new Vector3(v.x, v.y, v.z + z);

    public static Vector3 WithX(this Vector3 v, float x) => new Vector3(x, v.y, v.z);
    public static Vector3 WithY(this Vector3 v, float y) => new Vector3(v.x, y, v.z);
    public static Vector3 WithZ(this Vector3 v, float z) => new Vector3(v.x, v.y, z);

    public static Vector3 WithZ(this Vector2 v, float z = 0) => new Vector3(v.x, v.y, z);

    public static Vector3 RotateAround(this Vector3 point, Vector3 pivot, float zAngle) =>
        Quaternion.Euler(0, 0, zAngle) * (point - pivot) + pivot;

    public static float Scale(this float f, float min, float max) => f * (max - min) + min;

    public static T Roll<T>(this float f, T outcome1, T outcome2) => Rng.value < f ? outcome1 : outcome2;

    public static bool TimePassedSince(this float interval, float timeInitial) => Time.time - timeInitial > interval;

    public static T Random<T>(this T[] array)
    {
        if (array == null)
            throw new ArgumentNullException(nameof(array));
        if (array.Length == 0)
            throw new ArgumentException("Collection must not be empty.");

        return array[Rng.Range(0, array.Length)];
    }

    public static bool EnsureSingletonInitialized<TBehaviour>(this TBehaviour behaviour, ref TBehaviour instance)
        where TBehaviour : MonoBehaviour
    {
        if (instance != null)
        {
            Object.Destroy(behaviour.gameObject);
            return true;
        }

        instance = behaviour;
        Object.DontDestroyOnLoad(instance.gameObject);

        return false;
    }

    public static bool ApproxRgba(this Color c, Color t) =>
        Mathf.Approximately(c.r, t.r) && Mathf.Approximately(c.g, t.g) && Mathf.Approximately(c.b, t.b) &&
        Mathf.Approximately(c.a, t.a);

    public static Color WithAlpha(this Color color, float a) => new Color(color.r, color.g, color.b, a);
    public static Color AddAlpha(this Color color, float a) => new Color(color.r, color.g, color.b, color.a + a);
    public static Color AddRgb(this Color c, float r = 0, float g = 0, float b = 0) =>
        new Color(c.r + r, c.g + g, c.b + b);

    public static T RemoveLast<T>(this IList<T> source)
    {
        var i = source.Count - 1;
        var element = source[i];
        source.RemoveAt(i);

        return element;
    }

    public static InstanceId InstanceId(this Object obj) => new InstanceId(obj);

    public static void TryStop(this IEnumerator coroutine, MonoBehaviour behaviour)
    {
        if (coroutine != null)
        {
            behaviour.StopCoroutine(coroutine);
        }
    }

    public static bool HasComponent<T>(this Component self) where T : Component => self.GetComponent<T>() != null;

    public static IEnumerable<Transform> GetChildren(this Transform self)
    {
        for (var i = 0; i < self.childCount; i++)
            yield return self.GetChild(i);
    }
}
