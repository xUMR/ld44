using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class RoundManager : MonoBehaviour
{
    [SerializeField] private int _rounds;
    [SerializeField] private bool _canResurrect = true;

    private static RoundManager _instance;

    public static RoundManager Instance => _instance;

    private InstanceId _playerId;

    public int Rounds => _rounds;

    private void Awake()
    {
        this.EnsureSingletonInitialized(ref _instance);

        _playerId = GameObject.FindWithTag(Tags.Player).transform.InstanceId();
    }

    private void OnEnable()
    {
        Events.OnDeath += PlayerDeath;
        Events.OnRestartGame += RestartGame;

        SceneManager.sceneLoaded += SceneLoaded;
    }

    private void OnDisable()
    {
        Events.OnDeath -= PlayerDeath;
        Events.OnRestartGame -= RestartGame;

        SceneManager.sceneLoaded -= SceneLoaded;
    }

    private void RestartGame()
    {
        _playerId = GameObject.FindWithTag(Tags.Player).transform.InstanceId();
    }

    private void SceneLoaded(Scene scene, LoadSceneMode mode)
    {
        if (scene.name == Scenes.RoundEndMenu)
        {
            EndRoundMenu();
        }
        else if (scene.name == Scenes.ShopMenu)
        {
            ShopMenu();
        }
        else if (scene.name == Scenes.GameOverMenu)
        {
            GameOver();
        }
    }

    private void GameOver()
    {
        var text = GameObject.FindWithTag(Tags.CurrencyText).GetComponent<Text>();
        text.text = Strings.WalletAmount;
    }

    private void ShopMenu()
    {
        var text = GameObject.FindWithTag(Tags.CurrencyText).GetComponent<Text>();
        text.text = Strings.WalletAmount;
    }

    private void EndRoundMenu()
    {
        var text = GameObject.FindWithTag(Tags.CurrencyText).GetComponent<Text>();
        text.text = Strings.TotalAmount;
    }

    private void PlayerDeath(InstanceId id)
    {
        if (id != _playerId) return;

//        Events.RoundEnd();
        Events.GameOver();
//        Events.FadeOut();
    }
}
