public struct StaminaDrainData
{
    public readonly InstanceId Id;
    public readonly float Amount;

    public StaminaDrainData(InstanceId id, float amount)
    {
        Id = id;
        Amount = amount;
    }
}
