using System;
using UnityEngine;
using UnityEngine.UI;

public class Purchasable : MonoBehaviour
{
    [SerializeField] private int _cost;
    [SerializeField] private PurchasableType _type;

    private Button _button;

    public int Cost => _cost;
    public bool Available => ShopManager.Instance.Wallet.Currency > Cost;

    private GameObject _player;

    private void OnEnable()
    {
        if (_type == PurchasableType.None)
        {
            throw new ArgumentException("Purchasable type mustn't be None");
        }

        _player = _player == null ? GameObject.FindWithTag(Tags.Player) : _player;

        PrepareButton();
        _button.onClick.AddListener(BuyMe);

        Events.OnPurchase += Purchase;
    }

    private void OnDisable()
    {
        _button.onClick.RemoveListener(BuyMe);

        Events.OnPurchase -= Purchase;
    }

    private void PrepareButton()
    {
        _button = _button == null ? GetComponent<Button>() : _button;
        _button.interactable = Available;

        if (!Available) return;
        // make sure the player can afford

        if (_type == PurchasableType.Heal)
        {
            _button.interactable = _player.GetComponent<Health>().IsWounded;
        }
        else if (_type == PurchasableType.RepairShield)
        {
            var shield = _player.GetComponentsInChildren<Equipment>()[1];
            var shieldIsDamaged = !shield.IsBroken && shield.IsDamaged;
            _button.interactable = shieldIsDamaged;
        }
        else if (_type == PurchasableType.UpgradeShield)
        {
            var shield = _player.GetComponentsInChildren<Equipment>()[1];
            _button.GetComponentInChildren<Text>().text = shield.IsBroken
                ? "Buy New Shield"
                : "Upgrade Shield";
        }
    }

    private void Purchase(PurchasableType type, float cost)
    {
        PrepareButton();
    }

    private void BuyMe()
    {
        Events.Purchase(_type, _cost);
    }
}

public enum PurchasableType
{
    None,
    Heal,
    UpgradeSword,
    UpgradeShield,
    RepairShield,
    Resurrect
}
