public enum MouseButton
{
    None,
    LeftDown,
    RightDown,
    RightUp,
    Both,
}
