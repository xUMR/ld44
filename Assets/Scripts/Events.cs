using System;
using UnityEngine;

public static class Events
{
    public static event Action<Vector2> OnCameraShake;
    public static void CameraShake(Vector2 magnitude) => OnCameraShake?.Invoke(magnitude);

    public static event Action<InstanceId> OnSwing;
    public static void Swing(InstanceId id) => OnSwing?.Invoke(id);

    public static event Action<InstanceId> OnSwingOver;
    public static void SwingOver(InstanceId id) => OnSwingOver?.Invoke(id);

    public static event Action<InstanceId> OnBlock;
    public static void Block(InstanceId id) => OnBlock?.Invoke(id);

    public static event Action<InstanceId> OnUnblock;
    public static void Unblock(InstanceId id) => OnUnblock?.Invoke(id);

    public static event Action<StaminaDrainData> OnDrainStamina;
    public static void DrainStamina(StaminaDrainData data) => OnDrainStamina?.Invoke(data);

    public static event Action<MouseButton> OnMouseInput;
    public static void MouseInput(MouseButton button) => OnMouseInput?.Invoke(button);

    public static event Action<HitData> OnHit;
    public static void Hit(HitData hit) => OnHit?.Invoke(hit);

    public static event Action<InstanceId> OnDeath;
    public static void Death(InstanceId id) => OnDeath?.Invoke(id);

    public static event Action OnRoundStart;
    public static void RoundStart() => OnRoundStart?.Invoke();

    public static event Action OnRoundEnd;
    public static void RoundEnd() => OnRoundEnd?.Invoke();

    public static event Action OnGameOver;
    public static void GameOver() => OnGameOver?.Invoke();

    public static event Action<float> OnAddMoney;
    public static void AddMoney(float money) => OnAddMoney?.Invoke(money);

    public static event Action<InstanceId> OnStaminaDepleted;
    public static void StaminaDepleted(InstanceId id) => OnStaminaDepleted?.Invoke(id);

    public static event Action<InstanceId> OnThrustPeak;
    public static void ThrustPeak(InstanceId id) => OnThrustPeak?.Invoke(id);

    public static event Action<PurchasableType, float> OnPurchase;
    public static void Purchase(PurchasableType type, float cost) => OnPurchase?.Invoke(type, cost);

    public static event Action OnFadeOut;
    public static void FadeOut() => OnFadeOut?.Invoke();

    public static event Action OnFadeOutComplete;
    public static void FadeOutComplete() => OnFadeOutComplete?.Invoke();

    public static event Action OnFadeIn;
    public static void FadeIn() => OnFadeIn?.Invoke();

    public static event Action OnFadeInComplete;
    public static void FadeInComplete() => OnFadeInComplete?.Invoke();

    public static event Action OnSuccessfulCut;
    public static void SuccessfulCut() => OnSuccessfulCut?.Invoke();

    public static event Action OnSuccessfulBlock;
    public static void SuccessfulBlock() => OnSuccessfulBlock?.Invoke();

    public static event Action OnSuccessfulParry;
    public static void SuccessfulParry() => OnSuccessfulParry?.Invoke();

    public static event Action OnRestartGame;
    public static void RestartGame() => OnRestartGame?.Invoke();
}
