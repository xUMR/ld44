using System;
using UnityEngine;

public class Equipment : MonoBehaviour
{
    [SerializeField] private EquipmentType _type;
    [SerializeField] private float _staminaDrain;
    [SerializeField] private float _weight;
    [SerializeField] private int _hitPoints = int.MaxValue;
    [SerializeField] private float _damageUpgrade;

    private int _defaultHitPoints;
    private Transform _root;

    public EquipmentType Type => _type;
    public float StaminaDrain => _staminaDrain;
    public float Weight => _weight;
    public LayerMask? DamageLayer { get; private set; }
    private Damage Damage { get; set; }

    public bool IsDamaged => _hitPoints != _defaultHitPoints;
    public bool IsBroken => _hitPoints == 0;

    private void Awake()
    {
        _defaultHitPoints = _hitPoints;
        Damage = GetComponent<Damage>();
        DamageLayer = Damage != null ? Damage.DamageLayer : (LayerMask?) null;

        _root = transform.root;
        if (CompareTag(Tags.Sword))
        {
            _type = EquipmentType.Sword;
        }
        else if (CompareTag(Tags.Shield))
        {
            _type = EquipmentType.Shield;
        }
    }

    private void OnEnable()
    {
        Events.OnBlock += Block;
        Events.OnUnblock += Unblock;
        Events.OnSwing += Swing;
        Events.OnSwingOver += SwingOver;
        Events.OnPurchase += Purchase;
    }

    private void OnDisable()
    {
        Events.OnBlock -= Block;
        Events.OnUnblock -= Unblock;
        Events.OnSwing -= Swing;
        Events.OnSwingOver -= SwingOver;
        Events.OnPurchase -= Purchase;
    }

    private void Purchase(PurchasableType type, float cost)
    {
        if (!_root.gameObject.CompareTag(Tags.Player)) return;

        if (Type == EquipmentType.Sword && type == PurchasableType.UpgradeSword)
        {
            Damage.Upgrade(_damageUpgrade);
            return;
        }

        if (Type != EquipmentType.Shield) return;

        if (type == PurchasableType.UpgradeShield)
        {
            _defaultHitPoints += Mathf.RoundToInt(_damageUpgrade);
            Repair();
        }
        else if (type == PurchasableType.RepairShield)
        {
            Repair();
        }
    }

    private bool IsValidShield(InstanceId id) => IsValid(id, EquipmentType.Shield);
    private bool IsValidSword(InstanceId id) => IsValid(id, EquipmentType.Sword);

    private bool IsValid(InstanceId id, EquipmentType type)
    {
        if (_type != type) return false;
        if (_root.GetInstanceID() != id.Value) return false;
        if (_hitPoints == 0) return false;

        return true;
    }

    private void Block(InstanceId id)
    {
        if (!IsValidShield(id)) return;

        gameObject.layer = Layers.DefenceActive;
        DrainStamina(_staminaDrain);
    }

    private void Unblock(InstanceId id)
    {
        if (!IsValidShield(id)) return;

        gameObject.layer = Layers.DefencePassive;
    }

    private void Swing(InstanceId id)
    {
        if (!IsValidSword(id)) return;

        gameObject.layer = Layers.DamageActive;
        DrainStamina(_staminaDrain);
    }

    private void SwingOver(InstanceId id)
    {
        if (!IsValidSword(id)) return;

        gameObject.layer = Layers.DamagePassive;
    }

    private void DrainStamina(float amount)
    {
        var data = new StaminaDrainData(_root.InstanceId(), amount);
        Events.DrainStamina(data);
    }

    public void DamageEquipment(int amount)
    {
        _hitPoints = Math.Max(0, _hitPoints - amount);
        if (_hitPoints == 0)
        {
            var sprite = GetComponent<SpriteRenderer>();
            sprite.color = sprite.color.WithAlpha(0);
            gameObject.layer = Layers.DefencePassive;
            Events.Unblock(_root.InstanceId());
        }
    }

    public void Repair()
    {
        _hitPoints = _defaultHitPoints;
        var sprite = GetComponent<SpriteRenderer>();
        sprite.color = sprite.color.WithAlpha(1);
    }
}
