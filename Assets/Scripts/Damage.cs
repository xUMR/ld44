using UnityEngine;

public class Damage : MonoBehaviour
{
    [SerializeField] private float _amount;
    [SerializeField] private LayerMask _damageLayer;

    public float Amount => _amount;
    public LayerMask DamageLayer => _damageLayer;

    public void Upgrade(float value) => _amount += value;
}
