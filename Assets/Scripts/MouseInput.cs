using UnityEngine;

public class MouseInput : MonoBehaviour
{
    private bool _isDead;

    private InstanceId _rootId;

    private void OnEnable()
    {
        _rootId = transform.root.InstanceId();

        Events.OnDeath += PlayerDeath;
    }

    private void OnDisable()
    {
        Events.OnDeath -= PlayerDeath;
    }

    private void PlayerDeath(InstanceId id)
    {
        if (id != _rootId) return;

        _isDead = true;
    }

    private void Update()
    {
        if (_isDead) return;

        var rightClickHeldDown = Input.GetMouseButton(1);
        var rightClickDown = Input.GetMouseButtonDown(1);
        var rightClickUp = Input.GetMouseButtonUp(1);
        var leftClickDown = Input.GetMouseButtonDown(0);

        if (rightClickHeldDown && leftClickDown)
        {
            // todo shield bash
//            Events.MouseInput(MouseButton.Both);
        }
        else if (leftClickDown)
        {
            Events.MouseInput(MouseButton.LeftDown);
        }
        else if (rightClickDown)
        {
            Events.MouseInput(MouseButton.RightDown);
        }
        else if (rightClickUp)
        {
            Events.MouseInput(MouseButton.RightUp);
        }
    }
}
