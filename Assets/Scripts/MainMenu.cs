using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour
{
    [SerializeField] private SceneFader _fader;

    public void StartGame()
    {
        _fader.FadeToColor(() =>
        {
            SceneManager.LoadSceneAsync(Scenes.Test).completed += _ =>
            {
                if (GameManager.Instance != null)
                {
                    GameManager.Instance.GameState = GameState.Playing;
                    Events.RestartGame();
                    Events.FadeIn();
                }
            };
        });
    }
}
