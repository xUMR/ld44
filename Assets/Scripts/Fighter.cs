using System.Linq;
using UnityEngine;

public class Fighter : MonoBehaviour
{
    [SerializeField] private FighterConfig _config;
    [SerializeField] private int _level = 0;
    [SerializeField] private Animator _animator;
    [SerializeField] private AnimationClip[] _swingAnimations;
    [SerializeField] private AnimationClip[] _blockUnblockAnimations;

    private int[] _swingAnimHashArray;
    private int[] _blockAnimHashArray;

    public FighterConfig Config => _config;
    public int Level => _level;

    private void Awake()
    {
        _swingAnimHashArray = _swingAnimations
            .Select(clip => Animator.StringToHash(clip.name))
            .ToArray();

        _blockAnimHashArray = _blockUnblockAnimations
            .Select(clip => Animator.StringToHash(clip.name))
            .ToArray();
    }

    private void OnEnable()
    {
        Events.OnSwing += SwingAnimation;
        Events.OnBlock += BlockAnimation;
        Events.OnUnblock += UnblockAnimation;
//        Events.OnRoundEnd += RoundEnd;
    }

    private void OnDisable()
    {
        Events.OnSwing -= SwingAnimation;
        Events.OnBlock -= BlockAnimation;
        Events.OnUnblock -= UnblockAnimation;
//        Events.OnRoundEnd -= RoundEnd;
    }

//    private void RoundEnd()
//    {
//        if (CompareTag(Tags.Player)) return;
//
//        FighterSpawner.Instance.Return(this);
//    }

    private void BlockAnimation(InstanceId id)
    {
        if (transform.GetInstanceID() != id.Value) return;

//        _animator.SetTrigger("block");
        _animator.Play(_blockAnimHashArray[0]);
    }

    private void UnblockAnimation(InstanceId id)
    {
        if (transform.GetInstanceID() != id.Value) return;

//        _animator.SetTrigger("unblock");
        _animator.Play(_blockAnimHashArray[1]);
    }

    private void SwingAnimation(InstanceId id)
    {
        if (transform.GetInstanceID() != id.Value) return;

//        if (!CanSwing()) return;
//
//        var r = (Random.Range(0, 2) & 1) == 1;
//        _animator.SetTrigger(r ? "Hit1" : "Hit-1");

        var clipHash = _swingAnimHashArray.Random();
        _animator.Play(clipHash);
    }

    private void AnimationEventSwingOver()
    {
        Events.SwingOver(transform.InstanceId());
    }

    private void AnimationEventThrustPeak()
    {
        Events.ThrustPeak(transform.InstanceId());
    }

    private bool CanSwing() => !_animator.GetBool("Hit1") && !_animator.GetBool("Hit-1");
}
